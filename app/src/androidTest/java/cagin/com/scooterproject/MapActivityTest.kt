package cagin.com.scooterproject

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.rule.ActivityTestRule
import cagin.com.scooterproject.feature.MapsScooterActivity
import org.junit.Rule
import org.junit.Test

class MapActivityTest {
    @Rule
    @JvmField
    val activity = ActivityTestRule<MapsScooterActivity>(MapsScooterActivity::class.java)


    @Test
    fun listViewClickable() {
        Espresso.onView(ViewMatchers.withId(R.id.map)).perform(ViewActions.click())
    }

    @Test
    fun progressBarVisible() {
        Espresso.onView(ViewMatchers.withId(R.id.progressBar))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun mapVisible() {
        Espresso.onView(ViewMatchers.withId(R.id.map))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}