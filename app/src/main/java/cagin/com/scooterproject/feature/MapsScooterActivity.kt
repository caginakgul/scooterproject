package cagin.com.scooterproject.feature

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import cagin.com.scooterproject.R
import cagin.com.scooterproject.data.retrofit.model.ScooterModel
import cagin.com.scooterproject.data.retrofit.model.ScooterResponse
import cagin.com.scooterproject.databinding.ActivityMapsScooterBinding
import cagin.com.scooterproject.feature.fragment.BottomSheetFragment

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.android.AndroidInjection
import timber.log.Timber
import java.util.ArrayList
import javax.inject.Inject

//TODO move permission things to the PermissionUtils class
//TODO inject Fragment with SupportFragmentInjector
//TODO write some unit test.

class MapsScooterActivity : FragmentActivity(), OnMapReadyCallback,
    BottomSheetFragment.BottomSheetListener {

    lateinit var mMap: GoogleMap
    private lateinit var viewModel: MapsScooterViewModel
    private lateinit var binding: ActivityMapsScooterBinding
    private lateinit var scooterList: List<ScooterModel>
    private val BOTTOMSHEETTAG: String = "BottomSheet"
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        Timber.i("onCreate")
        super.onCreate(savedInstanceState)
        initBinding()
        viewModel.sendCarListRequest().observe(this,
            Observer<ScooterResponse> { networkResponse ->
                if(networkResponse != null){
                    hideProgress()
                    scooterList = networkResponse.data.scooters
                    setMarkers(scooterList)
                }
            })
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        userLocation()
    }

    private fun initBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps_scooter)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MapsScooterViewModel::class.java)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun setMarkers(scooterList: List<ScooterModel>) {
        for (scooter: ScooterModel in scooterList) {
            val scooterLocation = LatLng(scooter.location.lat, scooter.location.lng)
            val marker = mMap.addMarker(
                MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin))
                    .position(scooterLocation).title(scooter.license_plate)
            )
            marker?.tag = scooter.id
        }
        mMap.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    scooterList[0].location.lat, scooterList[0].location.lng
                ), 16f
            )
        )
        mMap.setOnMarkerClickListener { marker ->
            BottomSheetFragment.newInstance(scooterList.find { it.id == marker.tag }!!)
                .show(supportFragmentManager, BOTTOMSHEETTAG)
            true
        }
    }

    override fun onOptionClick(text: String) {
        Timber.i(text)
    }

    private fun userLocation() {
        val permission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        if (permission != PackageManager.PERMISSION_GRANTED) {
            Timber.w("Location permission is not granted.")
            makeRequest()
        } else {
            Timber.i("Location permission granted.")
            mMap.isMyLocationEnabled = true
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 101
        )
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            101 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Timber.w("Permission has been denied by user")
                } else {
                    Timber.i("Permission has been granted by user")
                    mMap.isMyLocationEnabled = true
                }
            }
        }
    }

    private fun hideProgress() {
        binding.progressBar.visibility = View.GONE
    }
}
