package cagin.com.scooterproject.feature

import androidx.lifecycle.ViewModelProvider
import cagin.com.scooterproject.base.ProjectViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class MapsScooterActivityModule {
    @Provides
    fun provideCarListViewModel(mapsScooterViewModel: MapsScooterViewModel): ViewModelProvider.Factory {
        return ProjectViewModelFactory(mapsScooterViewModel)
    }
}