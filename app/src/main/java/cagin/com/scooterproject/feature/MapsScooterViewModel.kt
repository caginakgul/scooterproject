package cagin.com.scooterproject.feature

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cagin.com.scooterproject.common.ConnectionUtil
import cagin.com.scooterproject.data.repository.ScooterListRepository
import cagin.com.scooterproject.data.retrofit.model.ScooterResponse
import io.reactivex.Observable
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class MapsScooterViewModel @Inject constructor(application: Application) : AndroidViewModel(application) {
    @Inject
    lateinit var scooterRepository: ScooterListRepository

    fun sendCarListRequest(): LiveData<ScooterResponse> {
        if(ConnectionUtil.isConnectedOrConnecting(getApplication())){
            Timber.i("Device is connected to the internet")
            return scooterRepository.sendScooterListRequest()
        }else{
            Timber.w("No internet connection.")
            val liveData: MutableLiveData<ScooterResponse> = MutableLiveData()
            liveData.value = null
            return liveData
        }
    }
}