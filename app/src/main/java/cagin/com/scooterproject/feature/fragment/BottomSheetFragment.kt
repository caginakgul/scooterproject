package cagin.com.scooterproject.feature.fragment


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import cagin.com.scooterproject.R
import cagin.com.scooterproject.common.Constants
import cagin.com.scooterproject.data.retrofit.model.ScooterModel
import cagin.com.scooterproject.databinding.FragmentBottomSheetBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BottomSheetFragment : BottomSheetDialogFragment() {
    private lateinit var binding: FragmentBottomSheetBinding
    private lateinit var bottomSheetListener: BottomSheetListener

    companion object {
        fun newInstance(scooterModel: ScooterModel): BottomSheetFragment {
            val args = Bundle()
            args.putParcelable(Constants.Data.SCOOTER_ITEM_KEY, scooterModel)
            val fragment = BottomSheetFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_bottom_sheet, container, false
        )
        val args = arguments
        val scooterItemData = args?.getParcelable<ScooterModel>(Constants.Data.SCOOTER_ITEM_KEY)
        binding.scooterItem = scooterItemData
        binding.executePendingBindings()
        return binding.root
    }

    interface BottomSheetListener {
        fun onOptionClick(text: String)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            bottomSheetListener = context as BottomSheetListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context?.toString())
        }
    }

}