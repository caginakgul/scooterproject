package cagin.com.scooterproject.common

class Constants {
    object Api {
        val BASE_URL = "https://qc05n0gp78.execute-api.eu-central-1.amazonaws.com/prod/"
    }
    object Data{
        const val SCOOTER_ITEM_KEY = "Scooter"
    }
}