package cagin.com.scooterproject.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cagin.com.scooterproject.common.ConnectionUtil
import cagin.com.scooterproject.data.retrofit.implementation.ScooterListImplementation
import cagin.com.scooterproject.data.retrofit.model.ScooterResponse
import retrofit2.Call
import retrofit2.Response
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class ScooterListRepository @Inject constructor(scooterListImp: ScooterListImplementation) {
    private val scooterListImplementation: ScooterListImplementation = scooterListImp
    private var timer: Timer = Timer()

    fun sendScooterListRequest(): LiveData<ScooterResponse> {
        val liveData: MutableLiveData<ScooterResponse> = MutableLiveData()
        timer.scheduleAtFixedRate(
            object : TimerTask() {
                override fun run() {
                    scooterListImplementation.sendScooterRequest()
                        .enqueue((object : retrofit2.Callback<ScooterResponse> {
                            override fun onFailure(call: Call<ScooterResponse>?, t: Throwable?) {
                                Timber.e("request failed %s", t?.message)
                                liveData.value = null
                            }

                            override fun onResponse(
                                call: Call<ScooterResponse>?,
                                response: Response<ScooterResponse>?
                            ) {
                                Timber.i("request successful %s", response?.body()?.data?.scooters?.size)
                                liveData.value = response?.body()
                            }
                        }))
                }
            },
            0, 60000
        )
        return liveData
    }

}