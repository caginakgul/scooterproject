package cagin.com.scooterproject.data.retrofit.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScooterModel(
    val id: String,
    val vin: String,
    val model: String,
    val market_id: String,
    val license_plate: String,
    val energy_level: Int,
    val distance_to_travel: Double,
    val location: ScooterLocation
    ): Parcelable