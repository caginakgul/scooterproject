package cagin.com.scooterproject.data.retrofit.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScooterLocation(
    val lng: Double,
    val lat: Double
): Parcelable