package cagin.com.scooterproject.data.retrofit.model

data class MetaModel (
    val server_time: String,
    val status: Int,
    val key: String
)