package cagin.com.scooterproject.data.retrofit.model

data class ScooterResponse (
    val data: DataModel,
    val meta: MetaModel
)