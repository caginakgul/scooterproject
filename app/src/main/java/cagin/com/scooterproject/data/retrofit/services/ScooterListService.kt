package cagin.com.scooterproject.data.retrofit.services

import cagin.com.scooterproject.data.retrofit.model.ScooterModel
import cagin.com.scooterproject.data.retrofit.model.ScooterResponse
import retrofit2.Call
import retrofit2.http.GET

interface ScooterListService {
    @GET("scooters")
    fun sendScooterRequest(): Call<ScooterResponse>
}