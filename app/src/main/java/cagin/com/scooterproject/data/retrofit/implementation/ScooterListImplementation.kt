package cagin.com.scooterproject.data.retrofit.implementation

import cagin.com.scooterproject.common.ConnectionUtil
import cagin.com.scooterproject.data.retrofit.model.ScooterModel
import cagin.com.scooterproject.data.retrofit.model.ScooterResponse
import cagin.com.scooterproject.data.retrofit.services.ScooterListService
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject

class ScooterListImplementation @Inject constructor(builder: Retrofit.Builder) : ScooterListService {
    private val scooterListService: ScooterListService = builder.build().create(ScooterListService::class.java)

    override fun sendScooterRequest(): Call<ScooterResponse> {
            return scooterListService.sendScooterRequest()
    }
}