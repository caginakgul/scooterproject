package cagin.com.scooterproject.data.retrofit.model

data class DataModel (
    val scooters: List<ScooterModel>
)