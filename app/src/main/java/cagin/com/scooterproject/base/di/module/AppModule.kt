package cagin.com.scooterproject.base.di.module

import cagin.com.scooterproject.ScooterApplication
import dagger.Module

@Module
class AppModule(val app: ScooterApplication)