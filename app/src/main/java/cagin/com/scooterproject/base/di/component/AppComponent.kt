package cagin.com.scooterproject.base.di.component

import android.app.Application
import cagin.com.scooterproject.ScooterApplication
import cagin.com.scooterproject.base.di.builder.ActivityBuilder
import cagin.com.scooterproject.base.di.module.AppModule
import cagin.com.scooterproject.data.di.RetrofitModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        RetrofitModule::class,
        AndroidInjectionModule::class,
        AppModule::class,
        ActivityBuilder::class]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: ScooterApplication)
}