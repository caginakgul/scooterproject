package cagin.com.scooterproject.base.di.builder

import cagin.com.scooterproject.feature.MapsScooterActivity
import cagin.com.scooterproject.feature.MapsScooterActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = [MapsScooterActivityModule::class])
    internal abstract fun contributeMapsScooterActivity(): MapsScooterActivity
}